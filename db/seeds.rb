# encoding: utf-8
# Autogenerated by the db:seed:dump task
# Do not hesitate to tweak this to your needs

[ { :name => "Breakfast", :model => "F01", :released_date => "2013-07-09", :file => "image-1.jpg" },
  { :name => "Roti", :model => "F02", :released_date => "2013-08-09", :file => "image-2.jpg" },
  { :name => "Flower", :model => "P01", :released_date => "2013-06-14", :file => "image-3.jpg" },
  { :name => "Water Fountain", :model => "P02", :released_date => "2013-04-19", :file => "image-4.jpg" },
  { :name => "Sunset", :model => "E01", :released_date => "2013-08-15", :file => "image-5.jpg" },
  { :name => "Dawn", :model => "E02", :released_date => "2013-08-29", :file => "image-6.jpg" }, ].each do |r|
  p = Product.create(r, without_protection: true)
  p[:file] = r[:file]
  p.save!
end
