class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.string :model
      t.date :released_date
      t.string :file

      t.timestamps
    end
  end
end
