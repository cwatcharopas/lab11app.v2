class ProductsController < ApplicationController
  before_action :find_product, only: [ :show, :edit, :update, :destroy ]
  before_action :get_product, only: [ :new ]
  before_action :get_products, only: [ :index, :new, :edit ]

  def index
  end

  def new
    respond_to do |format|
      format.html { render 'index' }
      format.js
    end
  end

  def edit
    respond_to do |format|
      format.html { render 'index' }
      format.js
    end
  end

  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        format.html { redirect_to products_path, notice: 'Product was successfully created.' }
        format.json { render 'show', status: :created, location: @product }
        format.js
      else
        format.html { render 'new' }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to products_path, notice: 'Product was successfully updated.' }
        format.json { head :no_content }
        format.js
      else
        format.html { render 'edit' }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url }
      format.json { head :no_content }
      format.js
    end
  end

  def cancel
    @product = params[:id] == "0" ? get_product : find_product
    respond_to do |format|
      format.html { redirect_to products_url }
      format.json { head :no_content }
      format.js
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def find_product
      @product = Product.find(params[:id])
    end

    def get_product
      @product = Product.new released_date: Time.now
    end
    def get_products
      @products = Product.all
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:name, :model, :released_date, :file)
    end
end
