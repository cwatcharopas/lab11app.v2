if <%= @product.new_record? %>
  loc = $("tr#product_new")
  loc.fadeTo 300, 0, ->
    loc.remove()
  $("#new_button").show()
else
  $("tr#product_<%= @product.id %>").replaceWith("<%= j render 'data_row', product: @product %>")
  loc = $("tr#product_<%= @product.id %>")
  loc.fadeTo 0, 0, ->
    loc.fadeTo 500, 1