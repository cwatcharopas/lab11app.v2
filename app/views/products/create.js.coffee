$("tr#form_entry").before "<%= j render 'data_row', product: @product %>"
rows = $("tr[id^=product_]")
if rows.size() > 10
  rows.first().find("td > div").slideUp 300, ->
    rows.first().remove()